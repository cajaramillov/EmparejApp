package com.newmaster.coencentrecemvp.data.local.crud;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.newmaster.coencentrecemvp.data.local.EmparejAppContract.ScorePlayerEntity;
import com.newmaster.coencentrecemvp.data.local.EmparejAppDBHelper;
import com.newmaster.coencentrecemvp.data.local.EmparejAppDBHelper.Tables;
import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.settings.Util;

public class ScorePlayerCrud
{
    private static EmparejAppDBHelper dataBase;
    private static ScorePlayerCrud instance = new ScorePlayerCrud();

    private ScorePlayerCrud() {}

    public static ScorePlayerCrud getInstance(Context contexto)
    {
        if (dataBase == null)
        {
            dataBase = new EmparejAppDBHelper(contexto);
        }

        return instance;
    }

    public Cursor getAllScorePlayers()
    {
        SQLiteDatabase db = dataBase.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s ORDER BY %s", Tables.SCORE_PLAYER, ScorePlayerEntity.NAME);
        return db.rawQuery(sql, null);
    }

    public Cursor getOneScorePlayer(String idScorePlayer)
    {
        SQLiteDatabase db = dataBase.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s WHERE %s = %s", Tables.SCORE_PLAYER, ScorePlayerEntity._ID, idScorePlayer);
        return db.rawQuery(sql, null);
    }

    public Cursor getTopScorePlayersWithTime(String level)
    {
        SQLiteDatabase db = dataBase.getReadableDatabase();
        String sql = String.format("SELECT * " +
                                   "FROM %s " +
                                   "WHERE %s = '%s' " +
                                   "ORDER BY %s DESC, %s ASC " +
                                   "LIMIT 5",
                                   Tables.SCORE_PLAYER,
                                   ScorePlayerEntity.LEVEL, level,
                                   ScorePlayerEntity.SCORE, ScorePlayerEntity.TIME);

        return db.rawQuery(sql, null);
    }

    public boolean insertScorePlayer(ScorePlayer scorePlayer)
    {
        SQLiteDatabase db = dataBase.getWritableDatabase();
        return db.insert(Tables.SCORE_PLAYER, null, scorePlayer.toContentValues()) > 0;
    }

    public boolean updateScorePlayer(ScorePlayer scorePlayer)
    {
        SQLiteDatabase db = dataBase.getWritableDatabase();
        String whereClause = String.format("%s=?", ScorePlayerEntity._ID);
        final String[] whereArgs = {String.valueOf(scorePlayer.getId())};
        return db.update(Tables.SCORE_PLAYER, scorePlayer.toContentValues(), whereClause, whereArgs)> 0;
    }

    public boolean deleteScorePlayer(String idScorePlayer)
    {
        SQLiteDatabase db = dataBase.getWritableDatabase();
        String whereClause = String.format("%s=?", ScorePlayerEntity._ID);
        final String[] whereArgs = {idScorePlayer};
        return db.delete(Tables.SCORE_PLAYER, whereClause, whereArgs) > 0;
    }
}

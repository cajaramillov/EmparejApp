package com.newmaster.coencentrecemvp.data.local;

import android.provider.BaseColumns;


public class EmparejAppContract
{
    public static abstract class ScorePlayerEntity implements BaseColumns
    {
        public static final String NAME = "name";
        public static final String SCORE = "score";
        public static final String TIME = "time";
        public static final String LEVEL = "level";
    }
}

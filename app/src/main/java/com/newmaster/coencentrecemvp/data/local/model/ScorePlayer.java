package com.newmaster.coencentrecemvp.data.local.model;

import android.content.ContentValues;
import android.database.Cursor;
import java.io.Serializable;
import java.util.UUID;

import com.newmaster.coencentrecemvp.data.local.EmparejAppContract.ScorePlayerEntity;

public class ScorePlayer implements Serializable
{
    private String id;
    private String name;
    private String score;
    private String time;
    private String level;

    public ScorePlayer() {}

    public ScorePlayer(String name, String score, String time, String level)
    {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.score = score;
        this.time = time;
        this.level = level;
    }

    public ScorePlayer(Cursor cursor)
    {
        this.setId(cursor.getString(cursor.getColumnIndex(ScorePlayerEntity._ID)));
        this.setName(cursor.getString(cursor.getColumnIndex(ScorePlayerEntity.NAME)));
        this.setScore(cursor.getString(cursor.getColumnIndex(ScorePlayerEntity.SCORE)));
        this.setTime(cursor.getString(cursor.getColumnIndex(ScorePlayerEntity.TIME)));
        this.setLevel(cursor.getString(cursor.getColumnIndex(ScorePlayerEntity.LEVEL)));
    }

    public ContentValues toContentValues()
    {
        ContentValues values = new ContentValues();
        values.put(ScorePlayerEntity.NAME, this.getName());
        values.put(ScorePlayerEntity.SCORE, this.getScore());
        values.put(ScorePlayerEntity.TIME, this.getTime());
        values.put(ScorePlayerEntity.LEVEL, this.getLevel());
        return  values;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {

        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getScore()
    {
        return score;
    }

    public void setScore(String score)
    {
        this.score = score;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }
}

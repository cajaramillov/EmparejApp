package com.newmaster.coencentrecemvp.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.newmaster.coencentrecemvp.data.local.EmparejAppContract.ScorePlayerEntity;


public class EmparejAppDBHelper extends SQLiteOpenHelper
{
    private static final String DATA_BASE_NAME = "emparejapp.db";
    private static final int VERSION = 1;

    public EmparejAppDBHelper(Context contexto)
    {
        super(contexto, DATA_BASE_NAME, null, VERSION);
        Context context = contexto;
    }

    public interface Tables
    {
        String SCORE_PLAYER = "score_player";
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);

        if (!db.isReadOnly())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {
                db.setForeignKeyConstraintsEnabled(true);
            }

            else
            {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        // Table SCORE_PLAYER
        sqLiteDatabase.execSQL(String.format("CREATE TABLE %s (" +
                                             "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                                             "%s TEXT NOT NULL," +
                                             "%s INTEGER NOT NULL," +
                                             "%s INTEGER NOT NULL," +
                                             "%s TEXT NOT NULL)",
                                             Tables.SCORE_PLAYER,
                                             ScorePlayerEntity._ID,
                                             ScorePlayerEntity.NAME,
                                             ScorePlayerEntity.SCORE,
                                             ScorePlayerEntity.TIME,
                                             ScorePlayerEntity.LEVEL));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Tables.SCORE_PLAYER);
        this.onCreate(sqLiteDatabase);
    }
}

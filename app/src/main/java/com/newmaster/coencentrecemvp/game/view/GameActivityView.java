package com.newmaster.coencentrecemvp.game.view;

import com.newmaster.coencentrecemvp.game.logic.GameBoard;

public interface GameActivityView
{
    void createGameBoard(GameBoard gameBoard);
    void updateGameBoard(GameBoard gameBoard, int posCard, boolean isFlip, boolean isActive);
    void updateGameScore1(int score);
    void updateGameScore2(int score);
    void updateColorScorePlayer(int turn);
    void activateEventsImageCards();
    void showGameTime(String gameTime);
    void showGamePlayers(String player1, String player2);
    void showSocialActivity();
    void enabledGameCards(boolean enabled);
    void playWinSong();
    void playLoseSong();
}

package com.newmaster.coencentrecemvp.game.logic;

import android.util.Log;
import com.newmaster.coencentrecemvp.R;
import java.util.Collections;
import java.util.Random;
import java.util.ArrayList;

public class GameBoard
{
    private final int[] drawableCards = { R.drawable.alemania, R.drawable.argentina, R.drawable.arabia_saudita,
                                          R.drawable.australia, R.drawable.belgica, R.drawable.brazil,
                                          R.drawable.colombia, R.drawable.corea_sur, R.drawable.costa_rica,
                                          R.drawable.croacia, R.drawable.dinamarca, R.drawable.egipto,
                                          R.drawable.espana, R.drawable.francia, R.drawable.inglaterra,
                                          R.drawable.iran, R.drawable.islandia, R.drawable.japon,
                                          R.drawable.mexico, R.drawable.marruecos, R.drawable.nigeria,
                                          R.drawable.panama, R.drawable.peru, R.drawable.polonia,
                                          R.drawable.portugal, R.drawable.rusia, R.drawable.senegal,
                                          R.drawable.serbia, R.drawable.suecia, R.drawable.suiza,
                                          R.drawable.tunez, R.drawable.uruguay };

    private ArrayList<GameCard> listCards;
    private int quantityCards;

    public GameBoard(int quantityCards)
    {
        this.quantityCards = quantityCards;
        this.listCards = new ArrayList<>();
        this.crateCardBoard();
    }

    public void crateCardBoard()
    {
        int contCards = 0;
        ArrayList<Integer> numbers = new ArrayList<>();
        this.listCards.clear();
        Random random = new Random();

        while (contCards < this.quantityCards)
        {
            int pos = random.nextInt(this.drawableCards.length);
            Log.e("Numero Aleatorio", String.valueOf(pos));

            if (!numbers.contains(pos))
            {
                numbers.add(pos);
                GameCard card = new GameCard(this.drawableCards[pos]);
                this.listCards.add(card);
                this.listCards.add(card);
                contCards++;
            }
        }

        Collections.shuffle(this.listCards);
        Collections.shuffle(this.listCards);
        Collections.shuffle(this.listCards);
    }

    public ArrayList<GameCard> getListCards()
    {
        return listCards;
    }

    public void setListCards(ArrayList<GameCard> listCards)
    {
        this.listCards = listCards;
    }

    public int getQuantityCards()
    {
        return quantityCards;
    }

    public void setQuantityCards(int quantityCards)
    {
        this.quantityCards = quantityCards;
    }
}

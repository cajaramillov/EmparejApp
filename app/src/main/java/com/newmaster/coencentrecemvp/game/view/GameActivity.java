package com.newmaster.coencentrecemvp.game.view;

import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.media.MediaPlayer;
import com.newmaster.coencentrecemvp.R;
import com.newmaster.coencentrecemvp.game.logic.GameBoard;
import com.newmaster.coencentrecemvp.game.logic.GameCard;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenter;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenterImpl;
import com.newmaster.coencentrecemvp.settings.Util;
import com.newmaster.coencentrecemvp.social.view.SocialActivity;
import java.util.ArrayList;


public class GameActivity extends AppCompatActivity implements GameActivityView
{
    private TextView tvGameTime;
    private TextView tvGameScore1;
    private TextView tvGameScore2;
    private TextView tvGamePlayer1;
    private TextView tvGamePlayer2;
    private GameActivityPresenter presenter;
    private ArrayList<ImageView> listImageCards;
    private MediaPlayer mpLoseSong;
    private MediaPlayer mpWinSong;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.mpLoseSong = MediaPlayer.create(this, R.raw.lose1);
        this.mpWinSong = MediaPlayer.create(this, R.raw.win2);
        this.tvGameTime = (TextView) findViewById(R.id.game_time);
        this.tvGameScore1 = (TextView) findViewById(R.id.game_score1);
        this.tvGameScore2 = (TextView) findViewById(R.id.game_score2);
        this.tvGamePlayer1 = (TextView) findViewById(R.id.player1);
        this.tvGamePlayer2 = (TextView) findViewById(R.id.player2);
        this.listImageCards = new ArrayList<>();
        this.presenter = new GameActivityPresenterImpl(this, this.getApplicationContext());
        this.presenter.loadGamePlayers();
        this.presenter.loadGameBoard();
        this.presenter.callGameTime();
    }

    @Override
    public void createGameBoard(GameBoard gameBoard)
    {
        createCardsGameBoard(gameBoard);
    }

    @Override
    public void updateGameScore1(int score)
    {
        String textGameScore = getText(R.string.game_score) + " " + String.valueOf(score);
        this.tvGameScore1.setText(textGameScore);
    }

    @Override
    public void updateGameScore2(int score)
    {
        String textGameScore = getText(R.string.game_score) + " " + String.valueOf(score);
        this.tvGameScore2.setText(textGameScore);
    }

    @Override
    public void updateColorScorePlayer(int turn)
    {
        if (turn == 1)
        {
            this.tvGamePlayer1.setTextColor(getResources().getColor(R.color.black));
            this.tvGameScore1.setTextColor(getResources().getColor(R.color.black));
            this.tvGamePlayer2.setTextColor(getResources().getColor(R.color.gray));
            this.tvGameScore2.setTextColor(getResources().getColor(R.color.gray));
        }

        else
        {
            this.tvGamePlayer2.setTextColor(getResources().getColor(R.color.black));
            this.tvGameScore2.setTextColor(getResources().getColor(R.color.black));
            this.tvGamePlayer1.setTextColor(getResources().getColor(R.color.gray));
            this.tvGameScore1.setTextColor(getResources().getColor(R.color.gray));
        }

    }

    @Override
    public void updateGameBoard(GameBoard gameBoard, int posCard, boolean isFlip, boolean isActive)
    {
        if (isActive)
        {
            if (!isFlip)
            {
                listImageCards.get(posCard).setImageResource(R.drawable.carta_mundial);
            }

            else
            {
                listImageCards.get(posCard).setImageResource(gameBoard.getListCards().get(posCard).getImage());
            }
        }

        else
        {
            listImageCards.get(posCard).setImageResource(R.drawable.white);
        }
    }

    @Override
    public void activateEventsImageCards()
    {
        View.OnClickListener imageClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int id = v.getId();
                presenter.eventClicGameCard(id - 1);
            }
        };

        for (ImageView imageViewCard: this.listImageCards)
        {
            imageViewCard.setOnClickListener(imageClickListener);
        }
    }

    @Override
    public void showGameTime(String gameTime)
    {
        String textGameTime = getText(R.string.game_time) + " " + gameTime;
        this.tvGameTime.setText(textGameTime);
    }

    @Override
    public void showGamePlayers(String player1, String player2)
    {
        this.tvGamePlayer1.setText(player1);
        this.tvGamePlayer2.setText(player2);
    }

    @Override
    public void showSocialActivity()
    {
        Intent intentSocial = new Intent(GameActivity.this, SocialActivity.class);
        startActivity(intentSocial);
    }

    @Override
    public void enabledGameCards(boolean enabled)
    {
        for (ImageView card: this.listImageCards)
        {
            card.setEnabled(enabled);
        }
    }

    @Override
    public void playWinSong()
    {
        this.mpWinSong.start();
    }

    @Override
    public void playLoseSong()
    {
        this.mpLoseSong.start();
    }

    private void createCardsGameBoard(GameBoard gameBoard)
    {
        LinearLayout linearLayoutGame = (LinearLayout) findViewById(R.id.linear_layout_game);
        linearLayoutGame.removeAllViews();
        this.listImageCards.clear();
        ArrayList<GameCard> listCards = gameBoard.getListCards();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int marginCard = (int) getResources().getDimension(R.dimen.margin_card);
        int sizeCard = (size.x - (4 * marginCard)) / 3;

        if (listCards.size() > 0)
        {
            LinearLayout imagesContainer = new LinearLayout(getApplicationContext());
            imagesContainer.setLayoutParams(new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            imagesContainer.setOrientation(LinearLayout.HORIZONTAL);
            imagesContainer.setGravity(Gravity.CENTER);

            for (int i = 0; i < listCards.size(); i++)
            {
                switch (gameBoard.getQuantityCards())
                {
                    case Util.LEVEL_EASY_CARDS:
                        if (i == 3 || i == 6)
                        {
                            linearLayoutGame.addView(imagesContainer);
                            imagesContainer = new LinearLayout(getApplicationContext());
                            imagesContainer.setLayoutParams(new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                            imagesContainer.setOrientation(LinearLayout.HORIZONTAL);
                            imagesContainer.setGravity(Gravity.CENTER);
                        }

                        break;

                    case Util.LEVEL_MIDDLE_CARDS:
                        if (i == 3 || i == 6 || i == 9)
                        {
                            linearLayoutGame.addView(imagesContainer);
                            imagesContainer = new LinearLayout(getApplicationContext());
                            imagesContainer.setLayoutParams(new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                            imagesContainer.setOrientation(LinearLayout.HORIZONTAL);
                            imagesContainer.setGravity(Gravity.CENTER);
                        }

                        break;

                    case Util.LEVEL_HARD_CARDS:
                        sizeCard = (size.x - (5 * marginCard)) / 4;

                        if (i == 4 || i == 8 || i == 12)
                        {
                            linearLayoutGame.addView(imagesContainer);
                            imagesContainer = new LinearLayout(getApplicationContext());
                            imagesContainer.setLayoutParams(new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                            imagesContainer.setOrientation(LinearLayout.HORIZONTAL);
                            imagesContainer.setGravity(Gravity.CENTER);
                        }

                        break;

                    default:
                        break;

                }

                ImageView imageCard = new ImageView(getApplicationContext());
                imageCard.setImageResource(R.drawable.carta_mundial);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(sizeCard, sizeCard);
                layoutParams.setMargins(marginCard, marginCard, 0, 0);
                imageCard.setLayoutParams(layoutParams);
                imageCard.setId(i + 1);
                this.listImageCards.add(imageCard);
                imagesContainer.addView(imageCard);
            }

            linearLayoutGame.addView(imagesContainer);
        }
    }
}

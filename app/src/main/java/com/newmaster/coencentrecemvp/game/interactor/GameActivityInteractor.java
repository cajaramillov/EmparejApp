package com.newmaster.coencentrecemvp.game.interactor;


public interface GameActivityInteractor
{
    void loadGameBoard();
    void loadGamePlayers();
    void callGameTime();
    void callBackGameTime(String time);
    void eventClicGameCard(int idCard);
}

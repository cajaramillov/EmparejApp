package com.newmaster.coencentrecemvp.game.logic;

public interface GameLogic
{
    void loadGameBoard(int quantityCards);
    void loadGamePlayers();
    void callBackGameTime(int time);
    void eventClicGameCard(int idCard);
}

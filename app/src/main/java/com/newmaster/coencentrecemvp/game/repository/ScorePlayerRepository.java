package com.newmaster.coencentrecemvp.game.repository;

import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;

public interface ScorePlayerRepository
{
    void addScorePlayer(ScorePlayer scorePlayer);
}

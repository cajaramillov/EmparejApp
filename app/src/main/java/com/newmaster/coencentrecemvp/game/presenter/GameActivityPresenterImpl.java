package com.newmaster.coencentrecemvp.game.presenter;

import android.content.Context;

import com.newmaster.coencentrecemvp.game.interactor.GameActivityInteractor;
import com.newmaster.coencentrecemvp.game.interactor.GameActivityInteractorImpl;
import com.newmaster.coencentrecemvp.game.logic.GameBoard;
import com.newmaster.coencentrecemvp.game.view.GameActivityView;

public class GameActivityPresenterImpl implements GameActivityPresenter
{
    private GameActivityInteractor interactor;
    private GameActivityView activityView;

    public GameActivityPresenterImpl(GameActivityView activityView, Context context)
    {
        this.activityView = activityView;
        this.interactor = new GameActivityInteractorImpl(this, context);
    }

    @Override
    public void loadGameBoard()
    {
        this.interactor.loadGameBoard();
    }

    @Override
    public void loadGamePlayers()
    {
        this.interactor.loadGamePlayers();
    }

    @Override
    public void sendGameBoard(GameBoard gameBoard)
    {
        this.activityView.createGameBoard(gameBoard);
        this.activityView.activateEventsImageCards();
    }

    @Override
    public void callGameTime()
    {
        this.interactor.callGameTime();
    }

    @Override
    public void showGameTime(String gameTime)
    {
        this.activityView.showGameTime(gameTime);
    }

    @Override
    public void showGamePlayers(String player1, String player2)
    {
        this.activityView.showGamePlayers(player1, player2);
    }

    @Override
    public void showSocialActivity()
    {
        this.activityView.showSocialActivity();
    }

    @Override
    public void eventClicGameCard(int idCard)
    {
        this.interactor.eventClicGameCard(idCard);
    }

    @Override
    public void updateGameBoard(GameBoard gameBoard, int posCard, boolean isFlip, boolean isActive)
    {
        this.activityView.updateGameBoard(gameBoard, posCard, isFlip, isActive);
    }

    @Override
    public void updateGameScore1(int score)
    {
        this.activityView.updateGameScore1(score);
    }

    @Override
    public void updateGameScore2(int score)
    {
        this.activityView.updateGameScore2(score);
    }

    @Override
    public void updateColorScorePlayer(int turn)
    {
        this.activityView.updateColorScorePlayer(turn);
    }

    @Override
    public void enabledGameCards(boolean enabled)
    {
        this.activityView.enabledGameCards(enabled);
    }

    @Override
    public void playWinSong()
    {
        this.activityView.playWinSong();
    }

    @Override
    public void playLoseSong()
    {
        this.activityView.playLoseSong();
    }
}

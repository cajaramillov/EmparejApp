package com.newmaster.coencentrecemvp.game.presenter;


import com.newmaster.coencentrecemvp.game.logic.GameBoard;

public interface GameActivityPresenter
{
    void loadGameBoard();
    void loadGamePlayers();
    void sendGameBoard(GameBoard gameBoard);
    void callGameTime();
    void showGameTime(String gameTime);
    void showGamePlayers(String player1, String player2);
    void showSocialActivity();
    void eventClicGameCard(int idCard);
    void updateGameBoard(GameBoard gameBoard, int posCard, boolean isFlip, boolean isActive);
    void updateGameScore1(int score);
    void updateGameScore2(int score);
    void updateColorScorePlayer(int turn);
    void enabledGameCards(boolean enabled);
    void playWinSong();
    void playLoseSong();
}

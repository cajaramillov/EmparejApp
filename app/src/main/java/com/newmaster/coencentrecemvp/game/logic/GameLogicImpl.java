package com.newmaster.coencentrecemvp.game.logic;

import android.content.Context;
import android.os.AsyncTask;

import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenter;
import com.newmaster.coencentrecemvp.game.repository.ScorePlayerRepository;
import com.newmaster.coencentrecemvp.game.repository.ScorePlayerRepositoryImpl;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;
import com.newmaster.coencentrecemvp.settings.Util;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameLogicImpl implements GameLogic
{
    private SharedPreferencesManager sharedPreferencesManager;
    private GameBoard gameBoard;
    private GameActivityPresenter presenter;
    private ScorePlayerRepository scorePlayerRepository;
    private TimerTask timerTask;
    private Timer timerGame;
    private String player1;
    private String player2;
    private String firstPlayer;
    private String secondPlayer;
    private int turn;
    private int scorePlayer1;
    private int scorePlayer2;
    private int minutes;
    private int seconds;
    private int totalSecondsGame;
    private int selectCard1;
    private int selectCard2;
    private int totalPairCards;
    private int clicCards;

    public GameLogicImpl(GameActivityPresenter presenter, String player1, String player2, Context context)
    {
        this.scorePlayerRepository = new ScorePlayerRepositoryImpl(presenter, context);
        this.sharedPreferencesManager = new SharedPreferencesManager(context);
        this.presenter = presenter;
        this.player1 = player1;
        this.player2 = player2;
        this.turn = 1;
        this.totalSecondsGame = 0;
        this.scorePlayer1 = 0;
        this.scorePlayer2 = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.selectCard1 = -1;
        this.selectCard2 = -1;
        this.clicCards = 0;
        this.timerTask = null;
        this.timerGame = null;
    }

    @Override
    public void loadGameBoard(int quantityCards)
    {
        this.totalPairCards = quantityCards;
        this.gameBoard = new GameBoard(quantityCards);
        this.presenter.sendGameBoard(this.gameBoard);
    }

    @Override
    public void loadGamePlayers()
    {
        String players[] = {this.player1, this.player2};
        Random random = new Random();
        int pos = random.nextInt(players.length);

        if (this.player1.equals(players[pos]))
        {
            this.presenter.showGamePlayers(player1, player2);
            this.firstPlayer = player1;
            this.secondPlayer = player2;
        }

        else
        {
            this.presenter.showGamePlayers(player2, player1);
            this.firstPlayer = player2;
            this.secondPlayer = player1;
        }

        this.sharedPreferencesManager.setKeyNameGamer1(this.firstPlayer);
        this.sharedPreferencesManager.setKeyNameGamer2(this.secondPlayer);
    }

    @Override
    public void callBackGameTime(final int time)
    {
        this.timerTask = new TimerTask()
        {
            android.os.Handler handler = new android.os.Handler();

            @Override
            public void run()
            {
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String min;
                        String sec;
                        seconds++;
                        totalSecondsGame++;

                        if (seconds == 59)
                        {
                            seconds = 0;
                            minutes++;
                        }

                        if (seconds <= 9)
                        {
                            sec = "0" + String.valueOf(seconds);
                        }

                        else
                        {
                            sec = String.valueOf(seconds);
                        }

                        if (minutes <= 9)
                        {
                            min = "0" + String.valueOf(minutes) + ":";
                        }

                        else
                        {
                            min = String.valueOf(minutes) + ":";
                        }

                        presenter.showGameTime(min + sec);

                        if (time > 0)
                        {
                            if (totalSecondsGame == time)
                            {
                                saveScorePlayers();
                                presenter.enabledGameCards(false);
                                stopTimer();
                                presenter.showSocialActivity();
                            }
                        }
                    }
                });
            }
        };

        this.timerGame = new Timer();
        this.timerGame.scheduleAtFixedRate(this.timerTask, 0, 1000);
    }

    @Override
    public void eventClicGameCard(int idCard)
    {
        if (this.gameBoard.getListCards().get(idCard).isVisible())
        {
            this.clicCards++;

            if (this.selectCard1 == idCard || this.selectCard2 == idCard)
            {
                this.clicCards--;
                this.presenter.updateGameBoard(this.gameBoard, idCard, false, true);
                return;
            }

            if (this.clicCards == 1)
            {
                this.selectCard1 = idCard;
                this.presenter.updateGameBoard(this.gameBoard, idCard, true, true);
                return;
            }

            if (this.clicCards == 2)
            {
                this.selectCard2 = idCard;
                this.presenter.updateGameBoard(this.gameBoard, idCard, true, true);
                new AsyncTaskFlags().execute();
            }
        }
    }

    public class AsyncTaskFlags extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            presenter.enabledGameCards(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                Thread.sleep(1000);
            }

            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);

            if (gameBoard.getListCards().get(selectCard1).getImage() == gameBoard.getListCards().get(selectCard2).getImage())
            {
                presenter.updateGameBoard(gameBoard, selectCard1, true, false);
                presenter.updateGameBoard(gameBoard, selectCard2, true, false);
                gameBoard.getListCards().get(selectCard1).setVisible(false);
                gameBoard.getListCards().get(selectCard2).setVisible(false);
                totalPairCards--;

                switch (turn)
                {
                    case 1:
                        scorePlayer1 += 100;
                        presenter.updateGameScore1(scorePlayer1);
                        break;

                    case 2:
                        scorePlayer2 += 100;
                        presenter.updateGameScore2(scorePlayer2);
                        break;
                }

                if (totalPairCards == 0)
                {
                    saveScorePlayers();
                    stopTimer();
                    presenter.showSocialActivity();
                }

                presenter.playWinSong();
            }

            else
            {
                presenter.updateGameBoard(gameBoard, selectCard1, false, true);
                presenter.updateGameBoard(gameBoard, selectCard2, false, true);

                switch (turn)
                {
                    case 1:
                        scorePlayer1 -= 2;
                        presenter.updateGameScore1(scorePlayer1);
                        turn = 2;
                        presenter.updateColorScorePlayer(turn);
                        break;

                    case 2:
                        scorePlayer2 -= 2;
                        presenter.updateGameScore2(scorePlayer2);
                        turn = 1;
                        presenter.updateColorScorePlayer(turn);
                        break;
                }

                presenter.playLoseSong();
            }

            clicCards = 0;
            selectCard1 = -1;
            selectCard2 = -1;
            presenter.enabledGameCards(true);
        }
    }

    private void saveScorePlayers()
    {
        this.sharedPreferencesManager.setKeyScoreGamer1(String.valueOf(scorePlayer1));
        this.sharedPreferencesManager.setKeyScoreGamer2(String.valueOf(scorePlayer2));
        this.sharedPreferencesManager.setTimeGame(String.valueOf(totalSecondsGame));

        ScorePlayer dataScorePlayer1 = new ScorePlayer();
        dataScorePlayer1.setName(firstPlayer);
        dataScorePlayer1.setScore(String.valueOf(scorePlayer1));
        dataScorePlayer1.setLevel(this.sharedPreferencesManager.getLevelGame());
        dataScorePlayer1.setTime(String.valueOf(totalSecondsGame));

        ScorePlayer dataScorePlayer2 = new ScorePlayer();
        dataScorePlayer2.setName(secondPlayer);
        dataScorePlayer2.setScore(String.valueOf(scorePlayer2));
        dataScorePlayer2.setLevel(this.sharedPreferencesManager.getLevelGame());
        dataScorePlayer2.setTime(String.valueOf(totalSecondsGame));

        scorePlayerRepository.addScorePlayer(dataScorePlayer1);
        scorePlayerRepository.addScorePlayer(dataScorePlayer2);
    }

    private void stopTimer()
    {
        if (this.timerTask != null)
        {
            this.timerTask.cancel();
        }

        if (this.timerGame != null)
        {
            this.timerGame.cancel();
        }
    }

}

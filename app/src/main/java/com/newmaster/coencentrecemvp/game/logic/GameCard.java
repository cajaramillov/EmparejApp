package com.newmaster.coencentrecemvp.game.logic;

public class GameCard
{
    private int image;
    private boolean flip;
    private boolean visible;

    public GameCard(){}

    public GameCard(int image)
    {
        this.image = image;
        this.flip = false;
        this.visible = true;
    }

    public int getImage()
    {
        return image;
    }

    public void setImage(int image)
    {
        this.image = image;
    }

    public boolean isFlip()
    {
        return flip;
    }

    public void setFlip(boolean flip)
    {
        this.flip = flip;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }
}

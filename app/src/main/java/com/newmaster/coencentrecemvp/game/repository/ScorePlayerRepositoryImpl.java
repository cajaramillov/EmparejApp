package com.newmaster.coencentrecemvp.game.repository;

import android.content.Context;
import android.util.Log;

import com.newmaster.coencentrecemvp.data.local.crud.ScorePlayerCrud;
import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenter;


public class ScorePlayerRepositoryImpl implements ScorePlayerRepository
{
    private GameActivityPresenter presenter;
    private ScorePlayerCrud scorePlayerCrud;

    public ScorePlayerRepositoryImpl(GameActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.scorePlayerCrud = ScorePlayerCrud.getInstance(context);
    }

    @Override
    public void addScorePlayer(ScorePlayer scorePlayer)
    {
        boolean add = this.scorePlayerCrud.insertScorePlayer(scorePlayer);

        if (add)
        {
            Log.e("Adicion BD", "True");
        }

        else
        {
            Log.e("Adicion BD", "False");
        }
    }
}

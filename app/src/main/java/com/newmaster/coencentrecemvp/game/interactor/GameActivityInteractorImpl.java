package com.newmaster.coencentrecemvp.game.interactor;

import android.content.Context;
import android.util.Log;

import com.newmaster.coencentrecemvp.game.logic.GameBoard;
import com.newmaster.coencentrecemvp.game.logic.GameLogic;
import com.newmaster.coencentrecemvp.game.logic.GameLogicImpl;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenter;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;
import com.newmaster.coencentrecemvp.settings.Util;


public class GameActivityInteractorImpl implements GameActivityInteractor
{
    private GameActivityPresenter presenter;
    private GameLogic gameLogic;
    private SharedPreferencesManager sharedPreferencesManager;

    public GameActivityInteractorImpl(GameActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        sharedPreferencesManager = new SharedPreferencesManager(context);
        String player1 = this.sharedPreferencesManager.getKeyNameGamer1();
        String player2 = this.sharedPreferencesManager.getKeyNameGamer2();
        this.gameLogic = new GameLogicImpl(this.presenter, player1, player2, context);
    }

    @Override
    public void loadGameBoard()
    {
        String levelGame = this.sharedPreferencesManager.getLevelGame();

        switch (levelGame)
        {
            case Util.LEVEL_EASY:
                this.gameLogic.loadGameBoard(Util.LEVEL_EASY_CARDS);
                break;

            case Util.LEVEL_MIDDLE:
                this.gameLogic.loadGameBoard(Util.LEVEL_MIDDLE_CARDS);
                break;

            case Util.LEVEL_HARD:
                this.gameLogic.loadGameBoard(Util.LEVEL_HARD_CARDS);
                break;

            default:
                this.gameLogic.loadGameBoard(Util.LEVEL_EASY_CARDS);
                break;
        }
    }

    @Override
    public void loadGamePlayers()
    {
        this.gameLogic.loadGamePlayers();
    }

    @Override
    public void callGameTime()
    {
        String time = this.sharedPreferencesManager.getPreferenceTime();

        if (time != null)
        {
            this.callBackGameTime(time);
        }
    }

    public void callBackGameTime(String time)
    {
        int timeMillis = 0;

        try
        {
            timeMillis = Integer.valueOf(time) / 1000;
        }

        catch (NumberFormatException err)
        {
            timeMillis = 0;
        }

        this.gameLogic.callBackGameTime(timeMillis);
    }

    @Override
    public void eventClicGameCard(int idCard)
    {
        this.gameLogic.eventClicGameCard(idCard);
    }
}

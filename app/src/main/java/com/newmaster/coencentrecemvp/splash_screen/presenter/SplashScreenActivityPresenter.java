package com.newmaster.coencentrecemvp.splash_screen.presenter;

public interface SplashScreenActivityPresenter
{
    void callNameGamers();
    void sendNameGamers(String nameGamer1, String nameGamer2);
    void setNameGamers(String nameGamer1, String nameGamer2);
}

package com.newmaster.coencentrecemvp.splash_screen.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.newmaster.coencentrecemvp.R;
import com.newmaster.coencentrecemvp.menu.view.MenuActivity;
import com.newmaster.coencentrecemvp.splash_screen.presenter.SplashScreenActivityPresenter;
import com.newmaster.coencentrecemvp.splash_screen.presenter.SplashScreenActivityPresenterImpl;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenActivityView
{
    private SplashScreenActivityPresenter presenter;
    private String NAME_GAMER_1 = "";
    private String NAME_GAMER_2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        presenter = new SplashScreenActivityPresenterImpl(this, this.getApplicationContext());
        presenter.callNameGamers();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                createConfigNamesGamersDialog();
            }
        }, 3000);
    }

    public void createConfigNamesGamersDialog()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = SplashScreenActivity.this.getLayoutInflater();
        final View viewAlert = inflater.inflate(R.layout.config_gamers, null);
        builder.setView(viewAlert);
        builder.setCancelable(false);
        builder.create();
        final AlertDialog d = builder.show();

        Button buttonSave = (Button) viewAlert.findViewById(R.id.btn_save_gamers);
        final EditText editTextGamer1 = (EditText) viewAlert.findViewById(R.id.et_gamer1);
        final EditText editTextGamer2 = (EditText) viewAlert.findViewById(R.id.et_gamer2);
        editTextGamer1.setText(NAME_GAMER_1);
        editTextGamer2.setText(NAME_GAMER_2);

        buttonSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!TextUtils.isEmpty(editTextGamer1.getText().toString()) && !TextUtils.isEmpty(editTextGamer2.getText().toString()))
                {
                    presenter.setNameGamers(editTextGamer1.getText().toString(), editTextGamer2.getText().toString());
                    d.dismiss();
                    Intent intentLogin = new Intent(SplashScreenActivity.this, MenuActivity.class);
                    startActivity(intentLogin);
                }

                else
                {
                    Toast.makeText(SplashScreenActivity.this, getString(R.string.empty_massage), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void receiveNameGamers(String nameGamer1, String nameGamer2)
    {
        this.NAME_GAMER_1 = nameGamer1;
        this.NAME_GAMER_2 = nameGamer2;
    }
}

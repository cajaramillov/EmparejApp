package com.newmaster.coencentrecemvp.splash_screen.presenter;

import android.content.Context;

import com.newmaster.coencentrecemvp.menu.interactor.MenuActivityInteractor;
import com.newmaster.coencentrecemvp.menu.interactor.MenuActivityInteractorImpl;
import com.newmaster.coencentrecemvp.menu.view.MenuActivityView;
import com.newmaster.coencentrecemvp.splash_screen.interactor.SplashScreenActivityInteractor;
import com.newmaster.coencentrecemvp.splash_screen.interactor.SplashScreenActivityInteractorImpl;
import com.newmaster.coencentrecemvp.splash_screen.view.SplashScreenActivityView;

public class SplashScreenActivityPresenterImpl implements SplashScreenActivityPresenter
{
    private SplashScreenActivityView activityView;
    private SplashScreenActivityInteractor interactor;

    public SplashScreenActivityPresenterImpl(SplashScreenActivityView activityView, Context context)
    {
        this.activityView = activityView;
        this.interactor = new SplashScreenActivityInteractorImpl(this, context);
    }

    @Override
    public void callNameGamers()
    {
        this.interactor.callNameGamers();
    }

    @Override
    public void sendNameGamers(String nameGamer1, String nameGamer2)
    {
        this.activityView.receiveNameGamers(nameGamer1, nameGamer2);
    }

    @Override
    public void setNameGamers(String nameGamer1, String nameGamer2)
    {
        this.interactor.setNameGamers(nameGamer1, nameGamer2);
    }
}

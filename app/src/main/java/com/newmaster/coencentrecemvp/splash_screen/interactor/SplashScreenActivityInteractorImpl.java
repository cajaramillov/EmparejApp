package com.newmaster.coencentrecemvp.splash_screen.interactor;

import android.content.Context;

import com.newmaster.coencentrecemvp.menu.presenter.MenuActivityPresenter;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;
import com.newmaster.coencentrecemvp.splash_screen.presenter.SplashScreenActivityPresenter;

public class SplashScreenActivityInteractorImpl implements SplashScreenActivityInteractor
{
    private SplashScreenActivityPresenter presenter;
    private SharedPreferencesManager sharedPreferencesManager;

    public SplashScreenActivityInteractorImpl(SplashScreenActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.sharedPreferencesManager = new SharedPreferencesManager(context);
    }

    @Override
    public void callNameGamers()
    {
        String nameGamer1 = sharedPreferencesManager.getKeyNameGamer1();
        String nameGamer2 = sharedPreferencesManager.getKeyNameGamer2();

        if (nameGamer1 != null && nameGamer2 != null)
        {
            this.presenter.sendNameGamers(nameGamer1, nameGamer2);
        }
    }

    @Override
    public void setNameGamers(String nameGamer1, String nameGamer2)
    {
        this.sharedPreferencesManager.setKeyNameGamer1(nameGamer1);
        this.sharedPreferencesManager.setKeyNameGamer2(nameGamer2);
    }
}

package com.newmaster.coencentrecemvp.score.view;

import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface ScoreActivityView
{
    void setDataScorePlayers(ArrayList<String> listDataHeader, HashMap<String, ArrayList<ScorePlayer>> listHash);
}

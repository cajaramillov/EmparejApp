package com.newmaster.coencentrecemvp.score.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import com.newmaster.coencentrecemvp.R;
import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.score.presenter.ExpandableListAdapter;
import com.newmaster.coencentrecemvp.score.presenter.ScoreActivityPresenter;
import com.newmaster.coencentrecemvp.score.presenter.ScoreActivityPresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoreActivity extends AppCompatActivity implements ScoreActivityView
{
    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHash;
    private ScoreActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ExpandableListView)findViewById(R.id.elv_score);
        this.presenter = new ScoreActivityPresenterImpl(this, this.getApplicationContext());
        this.presenter.loadDataScorePlayers();
    }

    @Override
    public void setDataScorePlayers(ArrayList<String> listDataHeader, HashMap<String, ArrayList<ScorePlayer>> listHash)
    {
        this.listAdapter = new ExpandableListAdapter(this, listDataHeader, listHash);
        this.listView.setAdapter(this.listAdapter);
    }
}

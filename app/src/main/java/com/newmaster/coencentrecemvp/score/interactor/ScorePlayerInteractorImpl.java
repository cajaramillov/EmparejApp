package com.newmaster.coencentrecemvp.score.interactor;

import android.content.Context;

import com.newmaster.coencentrecemvp.score.presenter.ScoreActivityPresenter;
import com.newmaster.coencentrecemvp.score.repository.ScoreRepository;
import com.newmaster.coencentrecemvp.score.repository.ScoreRepositoryImpl;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;

public class ScorePlayerInteractorImpl implements ScorePlayerInteractor
{
    private ScoreActivityPresenter presenter;
    private ScoreRepository repository;
    private SharedPreferencesManager sharedPreferencesManager;

    public ScorePlayerInteractorImpl(ScoreActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.repository = new ScoreRepositoryImpl(presenter, context);
        this.sharedPreferencesManager = new SharedPreferencesManager(context);
    }

    @Override
    public void loadDataScorePlayers()
    {
        this.repository.getTopScorePlayersWithTime();
    }
}

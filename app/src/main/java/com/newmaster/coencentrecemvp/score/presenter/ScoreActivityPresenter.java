package com.newmaster.coencentrecemvp.score.presenter;

import android.database.Cursor;


public interface ScoreActivityPresenter
{
    void loadDataScorePlayers();
    void setDataScorePlayers(Cursor cursorScoreEasy, Cursor cursorScoreMiddle, Cursor cursorScoreHard);
}

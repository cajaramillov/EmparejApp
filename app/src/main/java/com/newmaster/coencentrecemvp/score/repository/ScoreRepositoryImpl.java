package com.newmaster.coencentrecemvp.score.repository;

import android.content.Context;
import android.database.Cursor;

import com.newmaster.coencentrecemvp.data.local.crud.ScorePlayerCrud;
import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.game.presenter.GameActivityPresenter;
import com.newmaster.coencentrecemvp.score.presenter.ScoreActivityPresenter;
import com.newmaster.coencentrecemvp.settings.Util;

import java.util.ArrayList;

public class ScoreRepositoryImpl implements ScoreRepository
{
    private ScoreActivityPresenter presenter;
    private ScorePlayerCrud scorePlayerCrud;

    public ScoreRepositoryImpl(ScoreActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.scorePlayerCrud = ScorePlayerCrud.getInstance(context);
    }

    @Override
    public void getTopScorePlayersWithTime()
    {
        Cursor cursorScoreEasy = this.scorePlayerCrud.getTopScorePlayersWithTime(Util.LEVEL_EASY);
        Cursor cursorScoreMiddle = this.scorePlayerCrud.getTopScorePlayersWithTime(Util.LEVEL_MIDDLE);
        Cursor cursorScoreHard = this.scorePlayerCrud.getTopScorePlayersWithTime(Util.LEVEL_HARD);
        this.presenter.setDataScorePlayers(cursorScoreEasy, cursorScoreMiddle, cursorScoreHard);
    }
}

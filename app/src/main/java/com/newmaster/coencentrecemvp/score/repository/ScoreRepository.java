package com.newmaster.coencentrecemvp.score.repository;

import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;

public interface ScoreRepository
{
    void getTopScorePlayersWithTime();
}

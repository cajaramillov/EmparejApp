package com.newmaster.coencentrecemvp.score.presenter;

import android.content.Context;
import android.database.Cursor;

import com.newmaster.coencentrecemvp.data.local.model.ScorePlayer;
import com.newmaster.coencentrecemvp.score.interactor.ScorePlayerInteractor;
import com.newmaster.coencentrecemvp.score.interactor.ScorePlayerInteractorImpl;
import com.newmaster.coencentrecemvp.score.view.ScoreActivityView;
import com.newmaster.coencentrecemvp.settings.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoreActivityPresenterImpl implements ScoreActivityPresenter
{
    private ScoreActivityView activityView;
    private ScorePlayerInteractor interactor;

    public ScoreActivityPresenterImpl(ScoreActivityView activityView, Context context)
    {
        this.activityView = activityView;
        this.interactor = new ScorePlayerInteractorImpl(this, context);
    }

    @Override
    public void loadDataScorePlayers()
    {
        this.interactor.loadDataScorePlayers();
    }

    @Override
    public void setDataScorePlayers(Cursor cursorScoreEasy, Cursor cursorScoreMiddle, Cursor cursorScoreHard)
    {
        ArrayList<String> listDataHeader = new ArrayList<>();
        HashMap<String, ArrayList<ScorePlayer>> listHash = new HashMap<>();

        ArrayList<ScorePlayer> listScoreEasy = cursorToArray(cursorScoreEasy);
        ArrayList<ScorePlayer> listScoreMiddle = cursorToArray(cursorScoreMiddle);
        ArrayList<ScorePlayer> listScoreHard = cursorToArray(cursorScoreHard);

        listDataHeader.add(Util.LEVEL_EASY);
        listDataHeader.add(Util.LEVEL_MIDDLE);
        listDataHeader.add(Util.LEVEL_HARD);

        listHash.put(listDataHeader.get(0), listScoreEasy);
        listHash.put(listDataHeader.get(1), listScoreMiddle);
        listHash.put(listDataHeader.get(2), listScoreHard);

        this.activityView.setDataScorePlayers(listDataHeader, listHash);
    }

    private ArrayList<ScorePlayer> cursorToArray(Cursor cursor)
    {
        ScorePlayer scorePlayer;
        ArrayList<ScorePlayer> listScorePlayers = new ArrayList<>();

        while (cursor.moveToNext())
        {
            scorePlayer = new ScorePlayer(cursor);
            listScorePlayers.add(scorePlayer);
        }

        return listScorePlayers;
    }
}

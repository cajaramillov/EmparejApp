package com.newmaster.coencentrecemvp.menu.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.newmaster.coencentrecemvp.R;
import com.newmaster.coencentrecemvp.game.view.GameActivity;
import com.newmaster.coencentrecemvp.menu.presenter.MenuActivityPresenter;
import com.newmaster.coencentrecemvp.menu.presenter.MenuActivityPresenterImpl;
import com.newmaster.coencentrecemvp.score.view.ScoreActivity;
import com.newmaster.coencentrecemvp.settings.SettingsActivity;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;
import com.newmaster.coencentrecemvp.settings.Util;


public class MenuActivity extends AppCompatActivity implements MenuActivityView
{
    private MenuActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        presenter = new MenuActivityPresenterImpl(this, this.getApplicationContext());
    }

    // Se controla la pulsacion del boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(getString(R.string.exit_app))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.dismiss();
                        }
                    });

            builder.show();
        }

        return super.onKeyDown(keyCode, event);
    }

    public void onClickSettings(View view)
    {
        this.presenter.callSettings();
    }

    public void onClickGame(View view)
    {
        this.createGameLevelDialog();
    }

    public void onClickScore(View view)
    {
        this.presenter.callScore();
    }

    public void createGameLevelDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = MenuActivity.this.getLayoutInflater();
        View viewAlert = inflater.inflate(R.layout.custom_game, null);
        builder.setView(viewAlert);
        builder.setCancelable(false);
        builder.create();
        final AlertDialog d = builder.show();

        Button buttonEasy = (Button) viewAlert.findViewById(R.id.btn_level_easy);
        Button buttonMiddle = (Button) viewAlert.findViewById(R.id.btn_level_middle);
        Button buttonHard = (Button) viewAlert.findViewById(R.id.btn_level_hard);

        buttonEasy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                presenter.setLevelGame(Util.LEVEL_EASY);
                showPlay();
                d.dismiss();
            }
        });

        buttonMiddle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                presenter.setLevelGame(Util.LEVEL_MIDDLE);
                showPlay();
                d.dismiss();
            }
        });

        buttonHard.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                presenter.setLevelGame(Util.LEVEL_HARD);
                showPlay();
                d.dismiss();
            }
        });
    }

    @Override
    public void showSettings()
    {
        Intent settingsIntent = new Intent(MenuActivity.this, SettingsActivity.class);
        startActivity(settingsIntent);
    }

    @Override
    public void showScore()
    {
        Intent scoreIntent = new Intent(MenuActivity.this, ScoreActivity.class);
        startActivity(scoreIntent);
    }

    public void showPlay()
    {
        Intent gameIntent = new Intent(MenuActivity.this, GameActivity.class);
        startActivity(gameIntent);
    }

}

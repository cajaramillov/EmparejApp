package com.newmaster.coencentrecemvp.menu.interactor;


public interface MenuActivityInteractor
{
    void callSettings();
    void callScore();
    void setLevelGame(String levelGame);
}

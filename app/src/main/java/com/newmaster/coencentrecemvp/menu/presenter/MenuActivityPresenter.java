package com.newmaster.coencentrecemvp.menu.presenter;


public interface MenuActivityPresenter
{
    void callSettings();
    void callScore();
    void showSettings();
    void showScore();
    void setLevelGame(String gameLevel);
}

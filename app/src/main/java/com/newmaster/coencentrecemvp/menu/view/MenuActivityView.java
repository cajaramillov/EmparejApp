package com.newmaster.coencentrecemvp.menu.view;

public interface MenuActivityView
{
    void showSettings();
    void showScore();
}

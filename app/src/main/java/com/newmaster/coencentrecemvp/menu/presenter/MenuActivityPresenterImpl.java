package com.newmaster.coencentrecemvp.menu.presenter;

import android.content.Context;

import com.newmaster.coencentrecemvp.menu.interactor.MenuActivityInteractor;
import com.newmaster.coencentrecemvp.menu.interactor.MenuActivityInteractorImpl;
import com.newmaster.coencentrecemvp.menu.view.MenuActivityView;

public class MenuActivityPresenterImpl implements MenuActivityPresenter
{
    private MenuActivityView activityView;
    private MenuActivityInteractor interactor;

    public MenuActivityPresenterImpl(MenuActivityView activityView, Context context)
    {
        this.activityView = activityView;
        this.interactor = new MenuActivityInteractorImpl(this, context);
    }

    @Override
    public void callSettings()
    {
        this.interactor.callSettings();
    }

    @Override
    public void callScore()
    {
        this.interactor.callScore();
    }

    @Override
    public void showSettings()
    {
        this.activityView.showSettings();
    }

    @Override
    public void showScore()
    {
        this.activityView.showScore();
    }

    @Override
    public void setLevelGame(String levelGame)
    {
        this.interactor.setLevelGame(levelGame);
    }
}

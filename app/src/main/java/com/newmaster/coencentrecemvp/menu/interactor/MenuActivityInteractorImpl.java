package com.newmaster.coencentrecemvp.menu.interactor;

import android.content.Context;

import com.newmaster.coencentrecemvp.menu.presenter.MenuActivityPresenter;
import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;

public class MenuActivityInteractorImpl implements MenuActivityInteractor
{
    private MenuActivityPresenter presenter;
    private SharedPreferencesManager sharedPreferencesManager;
    private Context context;

    public MenuActivityInteractorImpl(MenuActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.context = context;
        this.sharedPreferencesManager = new SharedPreferencesManager(context);
    }

    @Override
    public void callSettings()
    {
        this.presenter.showSettings();
    }

    @Override
    public void callScore()
    {
        this.presenter.showScore();
    }

    @Override
    public void setLevelGame(String levelGame)
    {
        this.sharedPreferencesManager.setLevelGame(levelGame);
    }
}

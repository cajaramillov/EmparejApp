package com.newmaster.coencentrecemvp.social.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.newmaster.coencentrecemvp.R;
import com.newmaster.coencentrecemvp.social.presenter.SocialActivityPresenter;
import com.newmaster.coencentrecemvp.social.presenter.SocialActivityPresenterImpl;


public class SocialActivity extends AppCompatActivity implements SocialActivityView
{
    private TextView tvGameTime;
    private TextView tvGameScore1;
    private TextView tvGameScore2;
    private TextView tvGamePlayer1;
    private TextView tvGamePlayer2;
    private SocialActivityPresenter presenter;
    private String msgSocial;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        this.tvGameTime = (TextView) findViewById(R.id.game_time_social);
        this.tvGameScore1 = (TextView) findViewById(R.id.game_score1_social);
        this.tvGameScore2 = (TextView) findViewById(R.id.game_score2_social);
        this.tvGamePlayer1 = (TextView) findViewById(R.id.player1_social);
        this.tvGamePlayer2 = (TextView) findViewById(R.id.player2_social);
        this.presenter = new SocialActivityPresenterImpl(this, this.getApplicationContext());
        this.presenter.callDataFinalGame();
    }

    @Override
    public void showDataFinalGame(String player1, String player2, String scorePlayer1, String scorePlayer2, String gameTime)
    {
        String textGameTime = getText(R.string.game_time) + " " + gameTime;
        String textGameScore1 = getText(R.string.game_score) + " " + scorePlayer1;
        String textGameScore2 = getText(R.string.game_score) + " " + scorePlayer2;
        this.tvGameTime.setText(textGameTime);
        this.tvGamePlayer1.setText(player1);
        this.tvGamePlayer2.setText(player2);
        this.tvGameScore1.setText(textGameScore1);
        this.tvGameScore2.setText(textGameScore2);

        this.msgSocial = String.format("Puntaje Final \n%s: score: %s \n%s: score: %s \nTiempo: %s", player1, scorePlayer1, player2, scorePlayer2, gameTime);
    }

    public void onClickIntentFacebook(View view)
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, this.msgSocial);
        intent.setPackage("com.facebook.katana");
        startActivity(intent);
    }

    public void onClickIntentTwitter(View view)
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, this.msgSocial);
        intent.setPackage("com.twitter.android");
        startActivity(intent);
    }
}

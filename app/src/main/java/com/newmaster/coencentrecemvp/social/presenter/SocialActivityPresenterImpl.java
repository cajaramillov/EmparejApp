package com.newmaster.coencentrecemvp.social.presenter;

import android.content.Context;

import com.newmaster.coencentrecemvp.social.interactor.SocialActivityInteractor;
import com.newmaster.coencentrecemvp.social.interactor.SocialActivityInteractorImpl;
import com.newmaster.coencentrecemvp.social.view.SocialActivityView;

public class SocialActivityPresenterImpl implements SocialActivityPresenter
{
    private SocialActivityView activityView;
    private SocialActivityInteractor interactor;

    public SocialActivityPresenterImpl(SocialActivityView activityView, Context context)
    {
        this.activityView = activityView;
        this.interactor = new SocialActivityInteractorImpl(this, context);
    }

    @Override
    public void callDataFinalGame()
    {
        this.interactor.callDataFinalGame();
    }

    @Override
    public void showDataFinalGame(String player1, String player2, String scorePlayer1, String scorePlayer2, String gameTime)
    {
        this.activityView.showDataFinalGame(player1, player2, scorePlayer1, scorePlayer2, gameTime);
    }
}

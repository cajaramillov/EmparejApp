package com.newmaster.coencentrecemvp.social.interactor;

import android.content.Context;

import com.newmaster.coencentrecemvp.settings.SharedPreferencesManager;
import com.newmaster.coencentrecemvp.social.presenter.SocialActivityPresenter;

public class SocialActivityInteractorImpl implements SocialActivityInteractor
{
    private SocialActivityPresenter presenter;
    private SharedPreferencesManager sharedPreferencesManager;

    public SocialActivityInteractorImpl(SocialActivityPresenter presenter, Context context)
    {
        this.presenter = presenter;
        this.sharedPreferencesManager = new SharedPreferencesManager(context);
    }

    @Override
    public void callDataFinalGame()
    {
        String player1 = this.sharedPreferencesManager.getKeyNameGamer1();
        String player2 = this.sharedPreferencesManager.getKeyNameGamer2();
        String scorePlayer1 = this.sharedPreferencesManager.getKeyScoreGamer1();
        String scorePlayer2 = this.sharedPreferencesManager.getKeyScoreGamer2();
        String gameTime = this.sharedPreferencesManager.getTimeGame();
        this.presenter.showDataFinalGame(player1, player2, scorePlayer1, scorePlayer2, gameTime);
    }
}

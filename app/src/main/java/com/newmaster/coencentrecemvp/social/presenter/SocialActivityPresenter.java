package com.newmaster.coencentrecemvp.social.presenter;

public interface SocialActivityPresenter
{
    void callDataFinalGame();
    void showDataFinalGame(String player1, String player2, String scorePlayer1, String scorePlayer2, String gameTime);
}

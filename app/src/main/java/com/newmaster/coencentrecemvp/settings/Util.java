package com.newmaster.coencentrecemvp.settings;

public class Util
{
    public static final String KEY_PREF_TIME = "activate_time";
    public static final String KEY_NAME_GAMER_1 = "name_gamer_1";
    public static final String KEY_NAME_GAMER_2 = "name_gamer_2";
    public static final String KEY_SCORE_GAMER_1 = "score_gamer_1";
    public static final String KEY_SCORE_GAMER_2 = "score_gamer_2";
    public static final String LEVEL_GAME = "level_game";
    public static final String LEVEL_EASY = "easy";
    public static final String LEVEL_MIDDLE = "middle";
    public static final String LEVEL_HARD = "hard";
    public static final String KEY_GAME_TIME = "game time";
    public static final int LEVEL_EASY_CARDS = 4;
    public static final int LEVEL_MIDDLE_CARDS = 6;
    public static final int LEVEL_HARD_CARDS = 8;
}

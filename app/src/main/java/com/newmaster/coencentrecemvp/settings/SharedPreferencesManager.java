package com.newmaster.coencentrecemvp.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SharedPreferencesManager
{
    private SharedPreferences sharedPreferences;

    public SharedPreferencesManager(Context context)
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getPreferenceTime()
    {
        return sharedPreferences.getString(Util.KEY_PREF_TIME, null);
    }

    public String getKeyNameGamer1()
    {
        return sharedPreferences.getString(Util.KEY_NAME_GAMER_1, null);
    }

    public void setKeyNameGamer1(String keyNameGamer1)
    {
        sharedPreferences.edit().putString(Util.KEY_NAME_GAMER_1, keyNameGamer1).apply();
    }

    public String getKeyNameGamer2()
    {
        return sharedPreferences.getString(Util.KEY_NAME_GAMER_2, null);
    }

    public void setKeyNameGamer2(String keyNameGamer2)
    {
        sharedPreferences.edit().putString(Util.KEY_NAME_GAMER_2, keyNameGamer2).apply();
    }

    public String getKeyScoreGamer1()
    {
        return sharedPreferences.getString(Util.KEY_SCORE_GAMER_1, null);
    }

    public void setKeyScoreGamer1(String keyScoreGamer1)
    {
        sharedPreferences.edit().putString(Util.KEY_SCORE_GAMER_1, keyScoreGamer1).apply();
    }

    public String getKeyScoreGamer2()
    {
        return sharedPreferences.getString(Util.KEY_SCORE_GAMER_2, null);
    }

    public void setKeyScoreGamer2(String keyScoreGamer2)
    {
        sharedPreferences.edit().putString(Util.KEY_SCORE_GAMER_2, keyScoreGamer2).apply();
    }

    public String getLevelGame()
    {
        return sharedPreferences.getString(Util.LEVEL_GAME, null);
    }

    public void setLevelGame(String levelGame)
    {
        sharedPreferences.edit().putString(Util.LEVEL_GAME, levelGame).apply();
    }

    public String getTimeGame()
    {
        return sharedPreferences.getString(Util.KEY_GAME_TIME, null);
    }

    public void setTimeGame(String timeGame)
    {
        sharedPreferences.edit().putString(Util.KEY_GAME_TIME, timeGame).apply();
    }
}
